//
//  CustomButton.swift
//  app-swoosh
//
//  Created by Gray Zhen on 9/17/17.
//  Copyright © 2017 GrayStudio. All rights reserved.
//

import UIKit

class CustomButton: UIButton {

    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        layer.borderWidth = 2.7
        
        layer.borderColor = UIColor.white.cgColor
        
    }

}
