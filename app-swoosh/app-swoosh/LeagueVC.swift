//
//  LeagueVC.swift
//  app-swoosh
//
//  Created by Gray Zhen on 9/21/17.
//  Copyright © 2017 GrayStudio. All rights reserved.
//

import UIKit

class LeagueVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    @IBAction func onNextPressed(_ sender: Any) {
        performSegue(withIdentifier: "SkillVC", sender: self)
    }
    
    @IBAction func backToLeagueVC(segue: UIStoryboardSegue){
        
    }
    
}
